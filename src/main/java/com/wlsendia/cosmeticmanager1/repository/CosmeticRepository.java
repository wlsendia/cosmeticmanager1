package com.wlsendia.cosmeticmanager1.repository;

import com.wlsendia.cosmeticmanager1.entity.Cosmetic;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CosmeticRepository extends JpaRepository<Cosmetic, Long> {
}
