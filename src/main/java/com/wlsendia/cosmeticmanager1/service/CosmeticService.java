package com.wlsendia.cosmeticmanager1.service;

import com.wlsendia.cosmeticmanager1.entity.Cosmetic;
import com.wlsendia.cosmeticmanager1.repository.CosmeticRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CosmeticService {

    private final CosmeticRepository cosmeticRepository;

    public void setCosmeticService(String kind, String name, String owner) {
        Cosmetic addData = new Cosmetic();
        addData.setKind(kind);
        addData.setName(name);
        addData.setOwner(owner);

        cosmeticRepository.save(addData);

    }
}
