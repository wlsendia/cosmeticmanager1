package com.wlsendia.cosmeticmanager1.controller;

import com.wlsendia.cosmeticmanager1.model.CosmeticRequest;
import com.wlsendia.cosmeticmanager1.service.CosmeticService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor // 서버로 요청이 온 경우 여기서부터 코드 시작
@RequestMapping("/cosmetic")
public class CosmeticController {

    private final CosmeticService cosmeticService;

    @PostMapping("/data")
    public String setCosmetic(@RequestBody CosmeticRequest cosmeticRequest) {
        cosmeticService.setCosmeticService(cosmeticRequest.getKind(), cosmeticRequest.getName(), cosmeticRequest.getOwner());
        return "ok";
    }
}
