package com.wlsendia.cosmeticmanager1.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CosmeticRequest {

    private String kind;

    private String name;

    private String owner;
}
