package com.wlsendia.cosmeticmanager1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CosmeticManager1Application {

	public static void main(String[] args) {
		SpringApplication.run(CosmeticManager1Application.class, args);
	}

}
